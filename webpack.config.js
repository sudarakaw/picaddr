/* webpack.config.js: Webpack configuration
 *
 * Copyright 2019 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
 *
 * This program comes with ABSOLUTELY NO WARRANTY
 * This is free software, and you are welcome to redistribute it and/or modify
 * it under the terms of the BSD 2-clause License. See the LICENSE file for more
 * details.
 *
 */

const { resolve } = require('path')
    , Html = require('html-webpack-plugin')
    , CSS = require('mini-css-extract-plugin')
    , Copy = require('copy-webpack-plugin')
    , SW = require('serviceworker-webpack-plugin')

module.exports = (_, argv) => {
  const isDebug =  argv.d || argv.debug

      , source =
        { 'html': resolve(__dirname, 'src/index.html')
        , 'js': resolve(__dirname, 'src/main.js')
        , 'sw': resolve(__dirname, 'src/sw.js')
        }

      , htmlOptions =
        { 'template': source.html
        , 'minify':
          ( isDebug ? false :
            { 'collapseBooleanAttributes': true
            , 'collapseWhitespace': true
            , 'decodeEntities': true
            , 'html5': true
            , 'minifyCSS': true
            , 'minifyJS': true
            , 'removeAttributeQuotes': true
            , 'removeComments': true
            , 'removeRedundantAttributes': true
            , 'removeScriptTypeAttributes': true
            , 'removeStyleLinkTypeAttributes': true
            , 'useShortDoctype': true
            }
          )
        }

      , cssOptions =
        { 'hrm': isDebug
        , 'reloadAll': isDebug
        }

      , config =
        { 'mode': isDebug ? 'development' : 'production'
        , 'entry': { 'assets/js/main.js': source.js }
        , 'output':
          { 'filename': '[name]'
          , 'publicPath': ''
          }
        , 'module':
          { 'rules':
            [ { 'test': /\.elm$/
              , 'exclude': [ /elm-stuff/, /node_modules/ ]
              , 'use':
                [ { 'loader': 'elm-hot-webpack-loader' }
                , { 'loader': 'elm-webpack-loader'
                  , 'options':
                    { 'debug': isDebug
                    , 'optimize': !isDebug
                    }
                  }
                ]
              }
            , { 'test': /\.(sa|sc|c)ss$/
              , 'use':
                [ { 'loader': CSS.loader
                  , 'options': cssOptions
                  }
                , { 'loader': 'css-loader' }
                , { 'loader': 'sass-loader' }
                ]
              }
            ]
          }
        , 'plugins':
          [ new Html(htmlOptions)
          , new CSS( { 'filename': 'assets/css/style.css' } )
          , new Copy
            ( [ { 'from': resolve(__dirname, 'assets/')
                , 'to': 'assets/'
                , 'ignore': [ 'root/**/*' ]
                }
              , { 'from': resolve(__dirname, 'assets/root/*')
                , 'flatten': true
                }
              ]
            )
          , new SW
            ( { 'entry': source.sw
              , 'publicPath': ''
              }
            )
          ]
        }

  return config
}
