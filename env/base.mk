# env/base.mk: environment settings schema/defaults
#
# Copyright 2019 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
#
# This program comes with ABSOLUTELY NO WARRANTY
# This is free software, and you are welcome to redistribute it and/or modify
# it under the terms of the BSD 2-clause License. See the LICENSE file for more
# details.
#

# General
BASE_DIR       =$(realpath .)
SRC_DIR       ?=${BASE_DIR}/src
DIST_DIR      ?=${BASE_DIR}/dist

# HTTP (development) Server
HTTP_HOST     ?=127.0.0.1
HTTP_PORT     ?=5000

# Deployment settings
DEPLOY_URL    ?=
DEPLOY_USER   ?=
