module Item exposing (Item, ItemId, decode, encode, id, image, init, price)

-- src/Item.elm: Item module
--
-- Copyright 2019, 2020 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
--
-- This program comes with ABSOLUTELY NO WARRANTY
-- This is free software, and you are welcome to redistribute it and/or modify
-- it under the terms of the BSD 2-clause License. See the LICENSE file for more
-- details.
--

import Json.Decode as Decode exposing (Decoder)
import Json.Encode as Encode



-- TYPES


type alias ItemId =
    String


type Item
    = Item Internals


type alias Internals =
    { id : ItemId
    , price : Float
    , image : Maybe String
    }



-- ACCESSORS


id : Item -> String
id (Item internals) =
    internals.id


price : Item -> Float
price (Item internals) =
    internals.price


image : Item -> Maybe String
image (Item internals) =
    internals.image



-- INITIALIZERS


init : Float -> Maybe String -> Item
init price_ image_ =
    Internals "" price_ image_ |> Item



-- SERIALIZATIONS


pipe : List String -> Decoder a -> Decoder (a -> b) -> Decoder b
pipe keys decoder =
    Decode.map2 (|>) (Decode.at keys decoder)


decode : Decoder Item
decode =
    Decode.succeed Internals
        |> pipe [ "id" ] Decode.string
        |> pipe [ "price" ] Decode.float
        |> pipe [ "image" ] (Decode.maybe Decode.string)
        |> Decode.map Item


encode : Item -> Encode.Value
encode (Item internals) =
    [ ( "id", Encode.string "" )
    , ( "price", Encode.float internals.price )
    , ( "image", internals.image |> Maybe.map Encode.string |> Maybe.withDefault Encode.null )
    ]
        |> Encode.object
