/* src/main.js: entry point JS module for the app
 *
 * Copyright 2019, 2020 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
 *
 * This program comes with ABSOLUTELY NO WARRANTY
 * This is free software, and you are welcome to redistribute it and/or modify
 * it under the terms of the BSD 2-clause License. See the LICENSE file for more
 * details.
 *
 */

'use strict'

import PouchDB from 'pouchdb'
import { Elm } from './PicAddr.elm'
import runtime from 'serviceworker-webpack-plugin/lib/runtime';
import Camera from './Camera.js'
import ViewFinder from './ViewFinder.js'

import './style.sass'

const db = new PouchDB('picaddr-data')

    , flags = { 'version': VERSION || '0.0.0' }

    , app = Elm.PicAddr.init( { flags } )

    , addItem = db => item => db.post(item)
        .then(() => getRows(db))
        .then(updateModel)
        .catch(reportError)

    , removeItem = db => id => db.get(id)
        .then(db.remove)
        .then(() => getRows(db))
        .then(updateModel)
        .catch(reportError)

      // format the record to be compatible with Elm model
    , formatItem = ({ image, price, '_id': id }) =>
        ( { id
          , price
          , 'image': image || null
          }
        )

    , getRows = db => db.allDocs({ 'include_docs': true })
        .then(({ rows }) => rows)

    , updateModel = rows => {
        const docs = rows
                .map(({ doc }) => doc)
                // sort by created date
                .sort((a, b) => b.created - a.created)
                .map(formatItem)

        app.ports.updateModel.send(docs)
      }

    , reportError = err => console.error('ERROR!!', err)

if('serviceWorker' in navigator) {
  runtime.register()
}

getRows(db)
  .then(updateModel)
  .catch(reportError)

app.ports.addItem.subscribe(form => {
  const price = parseFloat(form.price)

  if(isNaN(price)) {
    return
  }

  addItem(db)({ price, 'image': form.image, 'created': Date.now() })
})

app.ports.openViewFinder.subscribe(() => {
  Camera
    .start()
    .then(cam => {
      ViewFinder
        .play(cam.getStream())
        .appendTo(document.body)
        .on('close', () => cam.stop())
        .on('capture', vf => {
          app.ports.updateFormImage.send(vf.dataUrl || null)

          vf.close()
        })
    })
    .catch(reportError)
})

app.ports.removeItem.subscribe(removeItem(db))
