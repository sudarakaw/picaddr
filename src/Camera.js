/* src/Camera.js: Camera library
 *
 * Copyright 2019 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
 *
 * This program comes with ABSOLUTELY NO WARRANTY
 * This is free software, and you are welcome to redistribute it and/or modify
 * it under the terms of the BSD 2-clause License. See the LICENSE file for more
 * details.
 *
 */

'use strict'

function initCamera() {
  let stream = null

  const start = () => {
          if(!navigator.mediaDevices || !navigator.mediaDevices.getUserMedia) {
            return Promise.reject('Multimedia support not available.')
          }

          return navigator.mediaDevices.getUserMedia
            ( { 'video': { 'facingMode': 'environment' }
              , 'audio': false
              }
            )
            .then(s => {
              stream = s

              return _internals
            })
        }

      , stop = () => {
          if(stream) {
            stream.getTracks().forEach(track => track.stop())

            stream = null
          }

          return _internals
        }

      , getStream = () => stream

      , _internals = { start, stop, getStream }

  return _internals
}

module.exports = initCamera()
