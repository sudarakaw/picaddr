port module PicAddr exposing (main)

-- src/PicAddr.elm: main module for the Elm application
--
-- Copyright 2019, 2020 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
--
-- This program comes with ABSOLUTELY NO WARRANTY
-- This is free software, and you are welcome to redistribute it and/or modify
-- it under the terms of the BSD 2-clause License. See the LICENSE file for more
-- details.
--

import Browser
import Browser.Navigation exposing (Key)
import FormatNumber
import FormatNumber.Locales exposing (usLocale)
import Html
    exposing
        ( Html
        , a
        , br
        , button
        , div
        , h1
        , h2
        , h3
        , hr
        , img
        , input
        , p
        , pre
        , small
        , span
        , strong
        , text
        )
import Html.Attributes
    exposing
        ( alt
        , class
        , disabled
        , href
        , maxlength
        , pattern
        , placeholder
        , src
        , target
        , type_
        , value
        )
import Html.Events exposing (onClick, onInput)
import Item exposing (Item, ItemId)
import Json.Decode as Decode
import Json.Encode as Encode
import Url exposing (Url)



-- MODEL


type alias Model =
    { list : List Item
    , form : Form
    , selected : Maybe ItemId
    , key : Key
    , infoScreen : Bool
    , flags : Flags
    }


type alias Form =
    { image : Maybe String
    , price : String
    , inputsValid : Bool
    }


type alias Flags =
    { version : String
    }


type Msg
    = UrlChanged Url
    | LinkClicked Browser.UrlRequest
    | UpdateModel (List Item)
    | SetPrice String
    | SubmitForm
    | SelectItem ItemId
    | RemoveSelection
    | RemoveItem ItemId
    | ShowInfo
    | HideInfo
    | OpenViewFinder
    | UserCapturedImageFromCamera (Maybe String)



-- MODEL helpers


emptyForm : Form
emptyForm =
    Form Nothing "" False



-- UPDATE


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        UrlChanged _ ->
            ( model, Cmd.none )

        LinkClicked _ ->
            ( model, Cmd.none )

        UpdateModel newList ->
            ( { model | list = newList }, Cmd.none )

        SetPrice val ->
            let
                { form } =
                    model

                newForm =
                    { form | price = val |> String.filter (\c -> Char.isDigit c || '.' == c) } |> validateForm
            in
            ( { model | form = newForm }, Cmd.none )

        SubmitForm ->
            if model.form.inputsValid then
                ( { model | form = emptyForm }
                , Item.init (model.form.price |> String.toFloat |> Maybe.withDefault 0) model.form.image
                    |> Item.encode
                    |> addItem
                )

            else
                ( model, Cmd.none )

        SelectItem id ->
            ( { model | selected = Just id }, Cmd.none )

        RemoveSelection ->
            ( { model | selected = Nothing }, Cmd.none )

        RemoveItem id ->
            ( model, removeItem id )

        ShowInfo ->
            ( { model | infoScreen = True }, Cmd.none )

        HideInfo ->
            ( { model | infoScreen = False }, Cmd.none )

        OpenViewFinder ->
            ( model, openViewFinder () )

        UserCapturedImageFromCamera image ->
            let
                from =
                    model.form
            in
            ( { model | form = { from | image = image } }, Cmd.none )



-- UPDATE helpers


validateForm : Form -> Form
validateForm form =
    let
        valid =
            form.price |> String.toFloat |> Maybe.map ((<) 0) |> Maybe.withDefault False
    in
    { form | inputsValid = valid }



-- SUBSCRIPTIONS


subscriptions : Model -> Sub Msg
subscriptions _ =
    Sub.batch
        [ updateModel
            (UpdateModel
                << Result.withDefault []
                << Decode.decodeValue (Decode.list Item.decode)
            )
        , updateFormImage UserCapturedImageFromCamera
        ]



-- VIEW


view : Model -> Browser.Document Msg
view { list, form, selected, infoScreen, flags } =
    { title = "Pic Adder"
    , body =
        if infoScreen then
            infoView flags.version

        else
            [ listView selected list
            , totalView list
            , formView form
            ]
    }


totalView : List Item -> Html Msg
totalView list =
    let
        total =
            list |> List.map Item.price |> List.foldl (+) 0

        totalText =
            if 1 > List.length list then
                "item list is empty"

            else
                total |> FormatNumber.format usLocale
    in
    div [ class "total-bar" ] [ text totalText ]


listItemView : ( Bool, Item ) -> Html Msg
listItemView ( selected, item ) =
    let
        divAttr =
            if selected then
                [ onClick RemoveSelection, class "selected" ]

            else
                [ onClick (item |> Item.id |> SelectItem) ]
    in
    div divAttr
        [ img [ alt "", src (item |> Item.image |> Maybe.withDefault placeHolderImg) ] []
        , span [] [ text (item |> Item.price |> FormatNumber.format usLocale) ]
        , button [ type_ "button", onClick (item |> Item.id |> RemoveItem) ] [ text "remove" ]
        ]


listView : Maybe ItemId -> List Item -> Html Msg
listView selected itemList =
    div [ class "item-list" ]
        (itemList |> List.map (isSelected selected >> listItemView))


formView : Form -> Html Msg
formView { price, image, inputsValid } =
    div [ class "entry-form" ]
        [ button [ type_ "button", onClick ShowInfo ] [ text "i" ]
        , button
            [ type_ "button", class "photo", onClick OpenViewFinder ]
            [ img [ alt "", src (image |> Maybe.withDefault "assets/img/button-white.svg") ] [] ]
        , input
            [ type_ "text"
            , maxlength 10
            , pattern "[0-9\\.]+"
            , placeholder "enter value"
            , value price
            , onInput SetPrice
            ]
            []
        , button
            [ type_ "button"
            , class "btn-add"
            , disabled (not inputsValid)
            , onClick SubmitForm
            ]
            [ text "Add" ]
        ]


infoView : String -> List (Html Msg)
infoView version =
    [ div [ class "info-screen" ]
        [ div [ class "info-body" ]
            [ h1 []
                [ text "Pic Adder"
                , br [] []
                , small [] [ text ("version " ++ version) ]
                ]
            , p [ class "copyright" ] [ text "Copyright 2019 Sudaraka Wijesinghe <sudaraka@sudaraka.org>" ]
            , hr [] []
            , h2 [] [ text "Licenses" ]
            , pre []
                [ text """
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""
                ]
            , h3 [] [ text "Application Icon" ]
            , p [] [ img [ src "assets/img/camera.svg" ] [] ]
            , p []
                [ text "Modified "
                , strong [] [ text "Nuvola camera.svg" ]
                , text " from From GNOME version of Nuvola via "
                , a [ href "https://commons.wikimedia.org/wiki/File:Nuvola_camera.svg", target "_new_win" ]
                    [ text "Wikimedia Commons" ]
                , text ", under "
                , a [ href "https://commons.wikimedia.org/wiki/File:Nuvola_camera.svg#Licensing", target "_new_win" ]
                    [ text "GNU Lesser General Public License" ]
                , text "."
                ]
            ]
        , button [ type_ "button", onClick HideInfo ] [ text "Close" ]
        ]
    ]



-- VIEW helpers


placeHolderImg : String
placeHolderImg =
    "assets/img/fallback.svg"


isSelected : Maybe ItemId -> Item -> ( Bool, Item )
isSelected selection item =
    selection
        |> Maybe.map (\id -> ( id == Item.id item, item ))
        |> Maybe.withDefault ( False, item )



-- PORTS


port updateModel : (Decode.Value -> msg) -> Sub msg


port updateFormImage : (Maybe String -> msg) -> Sub msg


port openViewFinder : () -> Cmd msg


port addItem : Encode.Value -> Cmd msg


port removeItem : ItemId -> Cmd msg



-- INIT


init : Flags -> Url -> Key -> ( Model, Cmd Msg )
init flags _ urlKey =
    ( Model [] emptyForm Nothing urlKey False flags, Cmd.none )



-- MAIN


main : Program Flags Model Msg
main =
    Browser.application
        { init = init
        , onUrlChange = UrlChanged
        , onUrlRequest = LinkClicked
        , subscriptions = subscriptions
        , update = update
        , view = view
        }
